import React from 'react'
import { CardList } from '../../components/cardList/cardList'
import MyCarousel from '../../components/carousel/carousel'
import { Container } from 'react-bootstrap'


export const Home = () => {
    return (
        <div>
            <MyCarousel />
            <Container>
                <CardList />
            </Container>
        </div>
    )
}