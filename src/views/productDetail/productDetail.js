import React, { useEffect, useState } from 'react';
import { Row, Col, Card, Button, Container } from 'react-bootstrap'
import axios from 'axios';
import './productDetail.css'
import { useParams } from 'react-router-dom';

const ProductDetail = ({ match }) => {

	const { id } = useParams()
	const [product, setProduct] = useState([]);
	useEffect(() => {
        (async () => {
			const { data } = await axios.get('https://mocki.io/v1/869dccb0-ecc2-4cd5-9198-58fb3ff1beb0')
			const foundItem = data.find(item => item.id === +id)
			setProduct(foundItem)
		})()  	  
    }, [id]);

	const [number, setNumber] = useState(0)
	const handleIncrement = () => {
		return setNumber(number + 1)
	}

	const handleDecrement = () => {
		return setNumber(number - 1)
	}

	return (
		<div className='CharacterDetail' style={{margin: 40 }}>
			
				<Container>
					<Row>
						<Col sm={6}>
							<Card>
								<Card.Img variant="top" src={product.img} className="mx-auto d-block" />
							</Card>
						</Col>
						<Col sm={6} className="">

							<h2>{product.title}</h2>
							<p>{product.category}</p>
							<h3>{product.price}</h3>
							<p>Cantidad:</p>
							<div className="d-flex align-content-space-between">
								<Button variant="secondary" onClick={handleDecrement}>-</Button><input type="text" value={number} className="form-control col-sm-3 text-center" /><Button variant="success" onClick={handleIncrement} >+</Button>
							</div>
							<Button variant="info" className="mt-3 col-sm-8">Añadir al carrito</Button>
							<Button className="mt-2 col-sm-8">Comprar</Button>

						</Col>
					</Row>
				</Container>
		</div>
	);
}

export default ProductDetail;