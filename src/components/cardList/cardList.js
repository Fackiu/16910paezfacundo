import React, { useState, useEffect } from 'react';
import { CardProducts } from '../cardProducts/cardProducts';
import axios from 'axios'
import './cardList.css'


export const CardList = () => {
	const [product, setProducts] = useState([])
    
    useEffect(() => {
          axios('https://mocki.io/v1/869dccb0-ecc2-4cd5-9198-58fb3ff1beb0')
		  .then(res => setProducts(res.data))
    }, []);
	

	return (
		<div className='CharacterList-container'>
			{product.map((prod) => {
				return (
					<div key={prod.id} >
						<CardProducts {...prod} />
					</div>
				);
			})}
		</div>
	    );
    }

