import { Carousel } from 'react-bootstrap'

// Nota: realizar aqui mapeo para img 

const MyCarousel = () => {
    return (
        <div>
            <Carousel fade>
                <Carousel.Item>
                    <img
                        className="d-flex w-100"
                        src="https://ilperfum.com/wp-content/uploads/2019/08/the-golden-secreIL-PERFUM-para-hombres-de-antonio-banderas-x100ml-original-santo-domingo-republica-dominicana.jpg"
                        alt="AB the secret"
                    />
                    <Carousel.Caption>
                        <h3>Antonio Banderas</h3>
                        <p>En este dia del padre aprovecha este producto.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-flex w-100"
                        src="http://d3ugyf2ht6aenh.cloudfront.net/stores/514/081/products/acquaest11-207afff42bf8a6b80015560262620546-640-0.png"
                        alt="AB the secret"
                    />
                    <Carousel.Caption>
                        <h3>Acqua di Colbert</h3>
                        <p>El mejor regalo para papá.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                
            </Carousel>
        </div>
)
}

export default MyCarousel