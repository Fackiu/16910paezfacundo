import {FormControl, Nav, Navbar, Form, NavDropdown} from 'react-bootstrap' 
import { Link } from 'react-router-dom'

const MyNavBar = () => {
    return (
        <div>
            <Navbar bg="light" variant="light" expand="lg">
                <Link to={'/'}>
                    <Navbar.Brand>Ficu-Distribuidora</Navbar.Brand>
                </Link>
                
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto" >
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Buscar" className="mr-sm-2" />
                    </Form>
                    <i class="far fa-shopping-cart"></i>
                    <NavDropdown title="Ingresar" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Iniciar sesión</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Registrarse</NavDropdown.Item>
                    </NavDropdown>
                </Navbar.Collapse>
            </Navbar>
            <Navbar bg="dark" variant="dark" className="p-2 justify-content-center">
                <Nav>
                    <NavDropdown title="Categorias">
                        <NavDropdown.Item href="#action/3.1">Perfumería</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Cosmetica</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Capilar</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Art. Varios</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Link className="mr-2"></Nav.Link>
                    <Nav.Link className="mr-2">Ofertas</Nav.Link>
                    <Nav.Link className="mr-2">Contacto</Nav.Link>
                </Nav>    
            </Navbar>
        </div>
    )
}

export default MyNavBar 
