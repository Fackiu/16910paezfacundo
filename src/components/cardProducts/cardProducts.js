import { Card, CardGroup } from 'react-bootstrap'
import './cardProducts.css'
//LINK ROUTER DOM
import { Link } from 'react-router-dom';

export const CardProducts = ({ id, category, img, title, price }) => {
    
    return (
            <div className="m-2">
                <CardGroup>
                    <Card>
                        <Card.Img variant="top" src={img} className="imgBlock mx-auto d-block" />
                        <Card.Body>
                            <hr />
                            <div key={id}>
                            <Link to={`/detail/${id}`}>
                                <strong>{title}</strong>
                                <p>{category}</p>
                                <Card.Text>{price}</Card.Text>
						    
                            </Link>
                            </div>
                        </Card.Body>
                    </Card>
                </CardGroup>
            </div>

    )
}


