// Components
import MyNavBar from './components/navbar/navbar'
import MyFooter from './components/footer/footer'

// Views
import { Home } from './views/home/home'
import ProductDetail from './views/productDetail/productDetail'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

const App = () => {
  return (
    <Router>
      <div className="App">
        <MyNavBar />
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/detail/:id' component={ProductDetail} />
          </Switch>
        <MyFooter />
      </div>
    </Router>
  )
   
}

export default App;
